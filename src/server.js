import colors from 'colors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';

import corsOptions from './config/cors';
import api from './router';

const PORT = process.argv[2] || 4000;
const API_BASE = 'localhost';
const API_SUFFIX = '/api';
const API_PATH = `${API_BASE}:${PORT}${API_SUFFIX}`;

console.log(colors.blue(`🏭  Enviroment is [${colors.yellow(process.env.NODE_ENV)}]`));

(async () => {
  try {
    // Configure Express HTTP Server
    const app = express();
    app.use(cors(corsOptions));
    app.use(cookieParser());
    app.use(bodyParser.json({ limit: '1000mb' }));
    app.use(bodyParser.urlencoded({ extended: true }));

    // Mount a router under API_SUFFIX.
    app.use(API_SUFFIX, api);

    // Start the server at configured port
    app.listen({ port: PORT }, (err) => {
      if (err) throw err;
      console.log(colors.blue(`🛰️  API listening at [${colors.yellow(API_PATH)}]`));
    });
  } catch (err) {
    console.error(colors.red(err));
  }
})();
