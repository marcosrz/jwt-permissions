const CLIENT_URL = 'localhost:3000';

export default {
  origin: CLIENT_URL,
  credentials: true,
};
