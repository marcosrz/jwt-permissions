import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config/auth';

export const createToken = (payload) => jwt.sign(payload, JWT_SECRET);

export const verifyAndDecode = (token) => jwt.verify(token, JWT_SECRET);

export const decode = (token) => jwt.decode(token);

export default { createToken, verifyAndDecode, decode };
