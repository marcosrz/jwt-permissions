import { TOKEN_KEY } from '../config/auth';
import { verifyAndDecode } from '../lib/auth';

/**
 * Middleware that looks for auth JWT Token in headers (Authorization Bearer)
 * and cookies, verifies, decodes and setS a req.auth property with the decoded
 * token payload.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const requireAuth = (req, res, next) => {
  const headerAuth = req.headers.authorization && req.headers.authorization.split(' ')[1];

  const cookieAuth = req.cookies[TOKEN_KEY];

  let auth = null;

  if (headerAuth) {
    auth = verifyAndDecode(headerAuth);
  }

  if (!auth && cookieAuth) {
    auth = verifyAndDecode(cookieAuth);
  }

  if (!auth) {
    res.status(403).send('Not logged in');
    return;
  }

  req.auth = auth;

  next();
};

/**
 * PATTERN: MIDDLEWARE FACTORY
 *
 * This middleware checks the user's permissions stored in req.auth.permissions
 * parameter and returns a 401 status code if ANY of the required permissions
 * is not present.
 *
 * I made it that way in order to provide a way to parametrize the middleware
 * instances, because It is intended to check for different permissions in
 * different routes.
 *
 * @param {Array of String} permissions
 */
export const requirePermissions = (permissions) => (req, res, next) => {
  if (!req.auth || !req.auth.permissions) {
    return res.status(401).send('Not enough permissionsx');
  }

  if (permissions.every((p) => req.auth.permissions[p] === true)) {
    return next();
  }

  res.status(401).send('Not enough permissions');
};

export default { requireAuth, requirePermissions };
