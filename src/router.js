import express from 'express';
import { TOKEN_KEY } from './config/auth';
import { createToken } from './lib/auth';

import { requireAuth, requirePermissions } from './middleware/auth';

const api = express.Router();

api.get('/', requireAuth, requirePermissions(['admin']), (req, res) =>
  res.send('<h1>Hello world</h1>'),
);

/**
 *   Generate a user token with implicit permissions and return it as JSON doc.
 *   Also sets up a cookie with the token.
 */
api.post('/login', (req, res) => {
  const { user, pass } = req.body;

  // TODO: Get user from data layer :D
  if (user === 'buat' && pass === 'defac') {
    const token = createToken({
      user,
      date: new Date(),
      permissions: { whoami: true, admin: true },
    });

    // Set token cookie for automatic browser auth
    res.cookie(TOKEN_KEY, token);

    // return token for other devices to store and put in Authorization Header
    return res.status(200).json({ token });
  }

  return res.status(401).send('Login failed!');
});

// This route requires the cookie or header to be present and valid
// And the user permissions encoded inside the token to include 'whoami' and 'admin'
api.get('/whoami', requireAuth, requirePermissions(['whoami', 'admin']), (req, res) => {
  res.json(req.auth);
});

export default api;
