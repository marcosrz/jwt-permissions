# JWT Permissions

¿Can't figure out how to protect your express routes with a "permission-driven" approach?

## Setup

Run `npm i` to install the project dependencies

## Run in development mode

Run `npm run dev` to start the development server. Dev mode starts nodemon, so any changes in the project source files will trigger a server restart.

## Make a prod build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Dependencies are not
installed in prod builds by default because the output is intended to be deployed, so if you want to run the build, you may
have to perform another `npm i` in your dist folder

## Running your prod build

Change your current dir to the dist folder. Run `npm run prod` to execute the production build. Terminal output will show some descriptive messages telling you that the app is now parametrized to work in produdcion mode.

## Tips

Have a look at router.js to have an idea about how the project works. 